# Automated simple hadoop cluster creator

## Basics:
Given that there are four nodes `master`, `slave1`, `slave2`, and `slave3`, installed with `jdk` and `hadoop-3.1.2`, and connected via passwordless `ssh`, then the script here can connect the nodes, to create a hadoop cluster.

## Prerequisites:
1. Java Development Kit
2. Hadoop-3.1.2 (installed at `~/`,i.e. home), for all nodes
3. Passwordless `ssh` connection from `master` to all `slave`s and the `master` itself. See [this](https://www.digitalocean.com/community/tutorials/how-to-spin-up-a-hadoop-cluster-with-digitalocean-droplets#step-4-%E2%80%94-set-up-ssh-for-each-node), to learn how to do that.

## Steps to perform:
1. Copy (or pull) the folder `transfer` into your `master` node.
2. Open `mainScript.py` from your editor, and edit the python dictionary `myDict`. Save the file.
3. If you want to see the `DEBUG` output, keep the `DEBUG` variable in the file as `True` otherwise, make it `False`.
4. Run the python file by `python3 mainScript.py` on your `master` node.
5. Run the hadoop cluster by doing the following on your `master` node:

    * `cd ~/hadoop-3.1.2`
    * `sudo ./bin/hdfs namenode -format`
    * `sudo ./sbin/start-dfs.sh`
    * `sudo ./sbin/start-yarn.sh`
6. Now, on your `master` node, run `<master-ip-address>:9870` on a browser, and check the health of the hadoop cluster, or simply do `./bin/hdfs dfsadmin -report | grep "Live datanodes"` from your `master`. It should return `Live datanodes (4)`. If it doesn't then something is wrong.

## Working:
To know about the working of the script, check [this](https://app.simplenote.com/publish/fj2lPF) out.