import os
from shutil import copyfile, move
import tarfile
import paramiko 
from subprocess import call, PIPE
debug = True
def maker(template,newfile,myDict):
    """
    Input:  Template file location
            Target file location
            Dictionary of variables and their values
    Output: None
    
    Functioning: Creates target files from the template files with variables replaced with their values as given in 
    the dict
    """
    with open(template,"r") as file, open(newfile,"w") as newfile:
        for line in file:
            txt = line 
            for key in myDict.keys():
                txt = txt.replace(key,myDict[key])
            newfile.write(txt)
def makeFiles(myDict,targetDirectory,templateDirectory):
    """
    Input:  variable:value dictionary
            templateDictionary, all files inside this directory are assumed to be templates and variables inside them are replaced with their values
            targetDirectory, Resulting files are put into this directory
    Output: None
    Functionality: Uses maker function on every file inside templateDirectory and puts the result in the targetDirectory
    """
    # Creates dir if it doesn't exist
    if(debug):
        print('		Creating files')
    if not os.path.isdir(targetDirectory):
        os.mkdir(targetDirectory)
        
    for filename in os.listdir(templateDirectory):
        maker(templateDirectory+"/"+filename,targetDirectory+"/"+filename,myDict)
        
def replaceConfigFiles(hadoopAddress,newConfigDirectory,sshDirectory):
    """
    Replaces configuration files at hadoopAddress (hadoop-installation/etc/hadoop/) with the new configuration files.
    If the current file is "config" then it should be put into "~/.ssh/"
    """
    if(debug):
        print('Replacing config files')
    for file in os.listdir(newConfigDirectory):
        if(file=='config'):
            copyfile(newConfigDirectory+"/"+file,sshDirectory+"/"+file)
        else:
            if os.path.exists(hadoopAddress+"/"+file):
                os.remove(hadoopAddress+"/"+file)
            copyfile(newConfigDirectory+"/"+file,hadoopAddress+"/"+file)
#Remaining: .tar the templates, scp templates to slaves, ssh into slaves, replace config files for the slaves.

def makeTar(targetName,sourceDir,destinationAddress):
    """
    Makes a tar file, having a name "targetName" from "sourceDir" and at "destinationAddress"
    """
    if(debug):
        print('		Making the tar :'+targetName)
    with tarfile.open(destinationAddress+"/"+targetName,"w:gz") as tar:
        tar.add(sourceDir, arcname=os.path.basename(sourceDir))
        
def dealWithMaster(myDict,temporaryConfigDir,templateDir,hadoopConfigDir,sshDir):
    """
    Master config files are made/replaced, given that we're currently inside the master node
    """
    if(debug):
        print('Dealing with master :')
    #Make sure the script is run as "sudo"
    os.system("sudo mkdir -p /usr/local/hadoop/hdfs/data")
    os.system("sudo chown -R "+myDict["$masteruser"]+":"+myDict["$masteruser"]+" /usr/local/hadoop/hdfs/data")
    makeFiles(myDict,temporaryConfigDir,templateDir)
    replaceConfigFiles(hadoopConfigDir,temporaryConfigDir,sshDir)
    if(debug):
        print("Dealt with master.") 

def dealWithSlave(myDict):
    """
    We're inside the slave, (via ssh) and have a .tar that has the templates. We need to untar the template file, replace/
    create the config files for the slave.
    
    myDict: Variable-value bindings
    localTarLocation: Location of the tar file, to be created
    tarMainFolder: Name of the main folder inside the tar
    tar--
        |
        --tarMainFolder--
                        |
                        -- Config files
                        |
                        -- script.py
    targetConfigDir: The folder in which the (to be) created config files live
    targetTemplateDir: The folder in which the templates (from which the config files are made) are kept
    tarName: Name of the (to be created) tar file
    """
    if(debug):
        print('Dealing with slaves.')
    for slaveNo in range(1,100):
        slaveIP = "$slave"+str(slaveNo)+"ip"
        slaveUser = "$slave"+str(slaveNo)+"user"
        slaveUserAddress = "$slave"+str(slaveNo)+"useraddress"
        if(debug):
	        print("	Dealing with slave :"+str(slaveNo))
        if slaveIP in myDict:
            myDict["$slaveuseraddress"] = myDict[slaveUserAddress]
            myDict["$slavelocaluser"] = myDict[slaveUser] 
            # Create the dirs?
            os.makedirs("slave_config/"+str(slaveNo)+"/config",exist_ok=True)
            makeFiles(myDict,"slave_config/"+str(slaveNo)+"/config/","templates/slave")
            # Fill the template slaveScript.py and create the release version of it, in slave_config/slave#/config/
            maker("templates/slaveScript.py","slave_config/"+str(slaveNo)+"/config/slaveScript.py",myDict)
            makeTar("config.tar.gz","slave_config/"+str(slaveNo)+"/config","slave_config/"+str(slaveNo))
            scpFilesViaCmd(myDict,slaveNo,"slave_config/"+str(slaveNo)+"/config.tar.gz",myDict[slaveUserAddress]+"/")
            goInSlave(myDict,slaveNo,myDict[slaveUserAddress]+"/config.tar.gz","config") #one
            if(debug):
                print(' Slave '+str(slaveNo)+' was succesfully dealt with.')
        else:
            print("No "+str(slaveNo)+" slave in myDict")
            break 
    
def scpFilesToSlave(myDict,slaveNo,localTarLocation,targetTarLocation):
    """
    Assumes that currently inside master node
    myDict has variable-value bindings
    slaveNo: 
    localTarLocation: Location of the tar to be transferred to the slave
    targetTarLocation: Target location of the tar, inside the slave
    """
    if(debug):
	    print(' Trying to sftp to slaveNo :'+str(slaveNo))
    slaveIP = "$slave"+str(slaveNo)+"ip"
    slaveUser = "$slave"+str(slaveNo)+"user"
    slaveUserAddress = "$slave"+str(slaveNo)+"useraddress"
    if slaveIP in myDict:
        transport = paramiko.Transport((myDict[slaveIP]))
        transport.connect(username=slaveUser)
        sftp = paramiko.SFTPClient.from_transport(transport)
        sftp.put(localTarLocation,targetTarLocation)
        sftp.close()
        transport.close()
        if(debug):
            print('    Succesfull sftp')
    else:
        print("No such slave :"+str(slaveNo))
def scpFilesViaCmd(myDict,slaveNo,localTarLocation,targetTarLocation):
    if(debug):
        print('Trying to scp the file :'+str(localTarLocation)+' into the slave number :'+str(slaveNo))
    slaveIP = "$slave"+str(slaveNo)+"ip"
    slaveUser = "$slave"+str(slaveNo)+"user"
    slaveUserAddress = "$slave"+str(slaveNo)+"useraddress"
    if slaveIP in myDict:
        call(['scp',localTarLocation,myDict[slaveUser]+'@'+myDict[slaveIP]+':'+targetTarLocation],stdout=PIPE,stderr=PIPE)
        if(debug):
            print('Succesfull scp of slave :'+str(slaveNo))
        
        
            
def goInSlave(myDict,slaveNo,tarFileLocation,tarMainFolder):
    """
    Assumes that tar has been scp'ed to all the slaves has been done, and the tar is present at "/home/user/" or "~/"
    Assumes that ssh is password-less
    SSHs to slaves, and untars the tar file (if exists, otherwise raises an error), and replaces/creates the config files.
    
    myDict is a dictionary of variables and their values
    tarFileLocation is a string, indicating the location of the .tar file
    tarMainFolder is a string, indicating the location of the main folder inside the tar file.
    """
    
    slaveIP = "$slave"+str(slaveNo)+"ip"
    slaveUser = "$slave"+str(slaveNo)+"user"
    slaveUserAddress = "$slave"+str(slaveNo)+"useraddress"
    if(debug):
	    print(' Going in slave :'+str(slaveNo))
    if  slaveIP in myDict:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        if(debug):
            print('==============================DEBUG=======================================================')
            print('slaveIP :'+myDict[slaveIP]+' slaveUser :'+myDict[slaveUser]+' tarFileLocation :'+tarFileLocation+' tarMainFolder :'+tarMainFolder)
            print('==============================DEBUG=======================================================')
        ssh.connect(myDict[slaveIP],username=myDict[slaveUser])
        if(debug):
            if(ssh.get_transport() is not None):
                print('SSHConnection :'+str(ssh.get_transport().is_active()))
                print('SSHAuthenticated :'+str(ssh.get_transport().is_authenticated()))
            else:
                print('No transport')
        stdin,stdout,stderr = ssh.exec_command("tar -xzvf "+tarFileLocation) 
        if(debug):
            print('tar result :'+str(stdout.readlines()))
            print('stderr :'+str(stderr.readlines()))
        stdin,stdout,stderr = ssh.exec_command("python3 "+tarMainFolder+"/slaveScript.py") 
        if(debug):
            print('Python result :'+str(stdout.readlines()))
            print('stderr :'+str(stderr.readlines()))
        stdin,stdout,stderr = ssh.exec_command("sudo mkdir -p /usr/local/hadoop/hdfs/data") 
        if(debug):
            print('Python result :'+str(stdout.readlines()))
            print('stderr :'+str(stderr.readlines()))
        stdin,stdout,stderr = ssh.exec_command("sudo chown -R "+myDict[slaveUser]+":"+myDict[slaveUser]+" /usr/local/hadoop/hdfs/data") 
        if(debug):
            print('Python result :'+str(stdout.readlines()))
            print('stderr :'+str(stderr.readlines()))
        ssh.close()
        if(debug):
            print(' ssh was successfull.')
    else:
        print('No such slave'+str(slaveNo))

def masterScript(myDict):
    if not (os.path.exists("master_config")):
	    os.mkdir("master_config")
    dealWithMaster(myDict,"master_config","templates/master",myDict["$masteruseraddress"]+"/hadoop-3.1.2/etc/hadoop",myDict["$masteruseraddress"]+"/.ssh/") #two, three
    dealWithSlave(myDict)


myDict = {
    "$masterip":"10.154.151.88",
    "$masteruseraddress":"/root",
    "$masteruser":"root",
    "$slave1ip":"10.154.151.252",
    "$slave2ip":"10.154.151.201",
    "$slave3ip":"10.154.151.173",
    "$javaaddress":"/usr/lib/jvm/java-8-openjdk-amd64",
    "$slave1useraddress":"/root",
    "$slave1user":"root",
    "$slave2useraddress":"/root",
    "$slave2user":"root",
    "$slave3useraddress":"/root",
    "$slave3user":"root",
    "$slaveuseraddress":"None",
    "$slavelocaluser":"None"
    
    
            }
masterScript(myDict) 
