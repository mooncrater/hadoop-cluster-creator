import os
from shutil import copyfile, move
debug = True 
def replaceConfigFiles(hadoopAddress,newConfigDirectory):
    """
    Replaces configuration files at hadoopAddress (hadoop-installation/etc/hadoop/) with the new configuration files.
    If the current file is "config" then it should be put into "~/.ssh/"
    """
    if(debug):
        print(str(os.listdir(newConfigDirectory)))
    for file in os.listdir(newConfigDirectory):
        if(file=='slaveScript.py'):
            continue
        else:
            if os.path.exists(hadoopAddress+"/"+file):
                os.remove(hadoopAddress+"/"+file)
            copyfile(newConfigDirectory+"/"+file,hadoopAddress+"/"+file)

replaceConfigFiles("$slaveuseraddress"+"/hadoop-3.1.2/etc/hadoop/","$slaveuseraddress"+"/config")
